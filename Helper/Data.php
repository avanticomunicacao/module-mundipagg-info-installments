<?php

declare(strict_types=1);

namespace Avanti\MundipaggInfoInstallments\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const PATH_TYPE_INSTALLMENTS_UNIQUE                     = 'payment/mundipagg_creditcard/installments_type';
    const PATH_INSTALLMENTS_IS_ACTIVE                       = 'payment/mundipagg_creditcard/installments_active';
    const PATH_INSTALLMENTS_NUMBER                          = 'payment/mundipagg_creditcard/installments_number';
    const PATH_INSTALLMENTS_IS_WITH_INTEREST                = 'payment/mundipagg_creditcard/installments_is_with_interest';
    const PATH_INSTALLMENTS_MIN_MOUNT                       = 'payment/mundipagg_creditcard/installment_min_amount';
    const PATH_INSTALLMENTS_INTEREST_RATE                   = 'payment/mundipagg_creditcard/installments_interest_rate_initial';
    const PATH_INSTALLMENTS_INTEREST_RATE_INCREMENTAL       = 'payment/mundipagg_creditcard/installments_interest_rate_incremental';
    const PATH_INSTALLMENTS_INTEREST_BY_ISSUER              = 'payment/mundipagg_creditcard/installments_interest_by_issuer';
    const PATH_INSTALLMENTS_MAX_WITHOUT_INTEREST            = 'payment/mundipagg_creditcard/installments_max_without_interest';

    protected $scopeConfig;

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
       return $this->getConfig(self::PATH_INSTALLMENTS_IS_ACTIVE);
    }

    public function getTypeInstallmentsUnique()
    {
        return $this->getConfig(self::PATH_TYPE_INSTALLMENTS_UNIQUE);
    }

    /**
     * @return int
     */
    public function getInstallmentsNumber()
    {
        return $this->getConfig(self::PATH_INSTALLMENTS_NUMBER);
    }

    /**
     * @return mixed
     */
    public function getInstallmentMinAmount()
    {
        return $this->getConfig(self::PATH_INSTALLMENTS_MIN_MOUNT);
    }

    /**
     * @return bool
     */
    public function isInterestByIssuer()
    {
        return $this->getConfig(self::PATH_INSTALLMENTS_INTEREST_BY_ISSUER);
    }

    /**
     * @return bool
     */
    public function isWithInterest()
    {
        return $this->getConfig(self::PATH_INSTALLMENTS_IS_WITH_INTEREST);
    }

    /**
     * @return float|int
     */
    public function getInterestRate()
    {
        return $this->getConfig(self::PATH_INSTALLMENTS_INTEREST_RATE);
    }

    /**
     * @return float|int
     */
    public function getInterestRateIncremental()
    {
        return $this->getConfig(self::PATH_INSTALLMENTS_INTEREST_RATE_INCREMENTAL);
    }

    /**
     * @return mixed
     */
    public function getInstallmentsMaxWithoutInterest()
    {
        return $this->getConfig(self::PATH_INSTALLMENTS_MAX_WITHOUT_INTEREST);
    }

    private function getConfig($path, $store = null)
    {
        if (!$store) {
             $store = ScopeInterface::SCOPE_STORE;
        }
        return $this->scopeConfig->getValue($path, $store);
    }
}
